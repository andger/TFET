function l2 = integration_limit(E, xC, V0, delta, TFET, Vds)
    %% Input device parameters
    L = TFET.L;
    lambda = TFET.lambda;
    V1 = TFET.V1;
    V2 = Vds + TFET.d1 + TFET.d2;
    
    %% V(x)
    V = V0*(sinh((pi/lambda).*(L-xC))./sinh(pi*L/lambda)) - V0+V1-delta-((V2-V0+V1-delta).*((sinh((pi*xC/lambda))./sinh(pi*L/lambda))));
    
    l2_cond = V + E;
    index = find(l2_cond < 0);
    l2 = xC(index(1));

end