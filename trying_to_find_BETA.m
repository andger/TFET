%TFET_short_channel
clear
close all
clc

%% Constants
qel = 1.602177e-19;                         % [C], elementary charge
epsilon0 = 8.854e-12;                       % [F/m]
k = qel * 8.617330350e-5;                   % [J/K]

L = 20e-9;
ti = 2e-9;
ts = 5e-9;
lambda = 9e-9;
epsilonr = 14.6;
epsilon = epsilonr * epsilon0;              % [F/m]
Nc = 8.7e+22;                               % [m^-3], CB effective DOS
Na = 3e19 * 1e6;
Cox = epsilon / ti;

Temp = 300;
V1 = 0.23;                                % [eV], effective band gap
d1 = 0.024;                               % [eV], source degeneracy

%% Find BETA parameter for charge
% 0 < BETA < pi/2 must hold
Vgs = 0.5;
Vds = 0.1;

% Graphical solution
figure;
fBETA = fplot(@(B) log(B) - log(cos(B)) + 2 * (ti/ts) * B .* tan(B), [0, pi/2], 'LineWidth', 1.25);
hold on;
F = qel*(Vgs-Vds-d1)/(2*k*Temp) - log(2/ts * sqrt(2*epsilon*k*Temp/qel^2/Nc));
plot(fBETA.XData, F * ones(size(fBETA.YData)), 'LineWidth', 1.25)
title('Graphical intersection for $\beta$', 'Interpreter', 'latex', 'FontSize', 14)
xlabel('$\beta$', 'Interpreter', 'latex', 'FontSize', 12)
legend({'$\log(\beta) - \log(\cos(\beta)) + 2\frac{t_i}{t_s}\beta\tan(\beta)$', ...
    '$q\frac{V_{gs}-V_{ds}-d_1}{2kT} - \log\left(\frac{2}{t_s}\sqrt{\frac{2\epsilon kT}{q^2N_c}}\right)$'}, ...
    'Interpreter', 'latex', 'Box', 'off', 'Location', 'best', 'FontSize', 12)

% Numerical solution (fsolve is more robust)
options = optimset('Display', 'off');
fBETA_num = @(B, Vgs_, Vds_, d1_) log(B) - log(cos(B)) + 2 * (ti/ts) * B .* tan(B) - qel*(Vgs_-Vds_-d1_)/(2*k*Temp) + log(2/ts * sqrt(2*epsilon*k*Temp/qel^2/Nc));
BETA = fsolve(@(B) fBETA_num(B, Vgs, Vds, d1), eps, options);
fprintf("Vgs = %f V\nVds = %f V\n\tBETA = %f\n", Vgs, Vds, BETA);

%% Inversion charge
Qinv = 4*k*Temp*epsilon/qel/ts * BETA * tan(BETA);  % [C/m^2]
fprintf("Qinv = %f C/m2\n", Qinv);

%% d2 -> V2 -> V0 -> Delta -> Wd
% Assuming me = mh, Nd = Na --> Nc = Nv --> d1 = d2
d2 = d1;
V2 = Vds + d1 + d2;
fprintf("V2 = qVds + d1 + d2 = %f eV\n", V2);

[V0] = V0compute_bis(L, Vgs-Qinv/Cox, V2);
fprintf("V0 = %f eV\n", V0);
delta = Vgs - Qinv/Cox - V0 + V1;
fprintf("Delta = q(Vgs -Qinv/Cox) - V0 + V1 = %f eV\n", delta);
alfa = 1e5*(qel^2*(Na*1e-6))/(2*epsilon/100);
Wd = 1e7 * sqrt(2*(epsilon/100)*delta/qel/(Na/1e6));
fprintf("Wd = %f cm\n", Wd);

%% Band diagram
L = L * 1e9;
lambda = lambda * 1e9;
xV = linspace(0, L, 1000);
xU = linspace(-Wd, 0, 1000);
V = @(x) V0 * sinh(pi*(L-x)/lambda)./sinh(pi*L/lambda) - V0 + V1 - delta ...
    - (V2 + V1 - V0 -delta) * sinh(pi*x/lambda)./sinh(pi*L/lambda);
U = @(x) -alfa * (x + Wd).^2;

figure; hold on;
plot(xU, U(xU), 'LineWidth', 1.25)
plot(xV, V(xV), 'LineWidth', 1.25)
grid on
title('Band diagram with de-bias model', 'Interpreter', 'latex', 'FontSize', 14)
xlabel('Position $x$ (nm)', 'Interpreter', 'latex', 'FontSize', 12)
ylabel('Energy (eV)', 'Interpreter', 'latex', 'FontSize', 12)
axis([min(xU), max(xV), 1.1*min(min(U(xU), V(xV))), 1.1*max(max(U(xU), V(xV)))])
legend({'$U(x)$', '$V(x)$'}, 'Interpreter', 'latex', 'Box', 'off', 'Location', 'best', 'FontSize', 12)

function [V0] = V0compute_bis(L, Vgs, V2)
    
    L = L * 1e9;
    
    % Fundamental constants
    qel = 1.602177e-19;                         % [C], electron charge
    epsilon0 = 8.854e-14;                       % [F/cm], vacuum dieletric constant
    
    % Technological device parameters
    epsilonr = 14.6;                            % [-], relative dielectric constant
    epsilon = epsilonr * epsilon0;              % [F/cm], dielectric constant
    Na = 3e19;                                  % [cm^-3], acceptor doping
    lambda = 9;                                 % [nm], tunneling window length
    
    % Potential parameters
    V1 = 0.23;                                  % [eV], effective band gap
    
    fun = @(V0) 1e7*(pi/lambda)*((V0*cosh(pi*L/lambda)+V2-Vgs)/(sinh(pi*L/lambda)))-sqrt(qel)*sqrt(2*Na*(Vgs+V1-V0)/epsilon);
    V_0 = 0.1;
    V0 = fzero(fun, V_0);
end