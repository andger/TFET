function VE = VE(x, E, L, lambda, V0, V1, V2, delta)

    VE = V0*(sinh((pi/lambda).*(L-x))./sinh(pi*L/lambda)) - V0 + V1 - delta ...
         - ((V2-V0+V1-delta).*((sinh((pi*x/lambda))./sinh(pi*L/lambda))))+E;
    VE = (1 + sign(VE))/2.*VE;
    
end