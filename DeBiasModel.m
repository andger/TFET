function Vg = DeBiasModel(TFET, Vgs, Vds)
    %% Constants
    qel = 1.602177e-19;                       % [C], elementary charge
    kb = 8.617330350e-5;                      % [eV/K], Boltzmann constant
    nmtocm = 1e-7;                            % [nm -> cm]
    
    %% Input arguments
    epsilon = TFET.epsilon;                   % [F/cm]
    Temp = TFET.Temp;                         % [K]
    ts = TFET.ts;                             % [cm]
    tox = TFET.tox;                           % [cm]
    d1 = TFET.d1;                             % [eV]
    Nc = TFET.Nc;                             % [cm^-3]
    
    %% Intermediate parameter BETA
    Vds_dep = (Vgs-Vds-d1)/(2*kb*Temp) - log(2/ts * sqrt(2*epsilon*kb*Temp/qel/Nc));
    fBETA = @(B) log(B) - log(cos(B)) + 2 * (tox/ts) * B .* tan(B) - Vds_dep;
    BETA = fzero(@(B) fBETA(B), [pi/1e12 pi/2-pi/1e12]);
    
    %% Inversion charge and modified gate voltage
    Qinv = 4 * kb * Temp * epsilon / ts * BETA * tan(BETA);
    Vg = Vgs - Qinv / epsilon * tox;
end